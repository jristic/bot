An explanation of the files (taken from the starter package readme):
Below is a fairly crude description of what each source file is used for:
    MyBot.cc - This file contains the main function for the bot, it just uses a Bot object to play a single game of Ants.

    Bot.h/cc - The Bot struct represents your bot in the game, the makeMoves function is where you ideally want to write your code for making moves each turn.

    State.h/cc - The State struct stores information about the current state of the game, take a look at the variables and functions for what is already provided, then start adding your own.

    Square.h - The Square struct is used to represent a square on the grid, it contains information like whether the square is water or food, if an ant is there what player it belongs to (this is set to -1 if no ant is currently there) and when it was last seen (this is set to 0 if the square has not been seen).

    Location.h - The Location struct is used to represent a location inside the grid, it simply contains two integers to refer to a row and column.

    Timer.h - The Timer struct allows you to check how long it has been since the start of the turn in milliseconds.

    Bug.h - The Bug struct allows you to easily debug information to files.

