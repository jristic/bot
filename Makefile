CC=g++
CFLAGS=-O3 -funroll-loops -c
LDFLAGS=-O2 -lm
VPATH=Src
SOURCES=Bot.cpp MyBot.cpp State.cpp
OBJECTS=$(SOURCES:.cc=.o)
EXECUTABLE=MyBot

#Uncomment the following to enable debugging
#CFLAGS+=-g -DDEBUG

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o: *.h
	$(CC) $(CFLAGS) $< -o $@

clean:
	-rm -f ${EXECUTABLE} ${OBJECTS} *.d
	-rm -f Debug/debug.txt

.PHONY: all clean
