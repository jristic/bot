#ifndef TIMER_H_
#define TIMER_H_

#ifdef _WIN32
#include <time.h>
#include <sys/timeb.h>
#else
#include <sys/time.h>
#endif

/*
	struct for checking how long it has been since the start of the turn.
*/
#ifdef _WIN32 //Windows timer (DON'T USE THIS TIMER UNLESS YOU'RE ON WINDOWS!)

#include <io.h>
#include <windows.h>

struct Timer
{
	LARGE_INTEGER freq;
	LARGE_INTEGER startC;
	LARGE_INTEGER stopC;
	long long time_ns;

	Timer()
	{

	};

	void start()
	{
		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&startC);   
	};

	double getTime()
	{
		QueryPerformanceCounter(&stopC);
		time_ns = (stopC.QuadPart - startC.QuadPart) * 1000000000 / freq.QuadPart;
		return (double)(time_ns/1000000.0);
	};
};

#else //Mac/Linux Timer

struct Timer
{
	timeval timer;
	double startTime, currentTime;

	Timer()
	{

	};

	//starts the timer
	void start()
	{
		gettimeofday(&timer, NULL);
		startTime = timer.tv_sec+(timer.tv_usec/1000000.0);
	};

	//returns how long it has been since the timer was last started in milliseconds
	double getTime()
	{
		gettimeofday(&timer, NULL);
		currentTime = timer.tv_sec+(timer.tv_usec/1000000.0);
		return (currentTime-startTime)*1000.0;
	};
};

#endif


#endif //TIMER_H_
