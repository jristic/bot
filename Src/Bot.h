#ifndef BOT_H_
#define BOT_H_

#include "State.h"

/*
 This struct represents your bot in the game of Ants
 */
struct Bot
{
	State state;
	
	Bot();
	
	void playGame();    //plays a single game of Ants
	
	void moveDirection(const Location& antLoc, int dir);
	void moveLocation(const Location& antLoc, const Location& targetLoc);
	
	bool isOccupied(const Location& loc) const;
	
	int locationToInt(const Location& loc) const;
	Location intToLocation(int loc) const;

	//makes moves for a single turn
	void makeMoves();
	//indicates to the engine that it has made its moves
	void endTurn();
};

#endif //BOT_H_
