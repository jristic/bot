#include "Bot.h"

#include <string.h>
#include <list>

using namespace std;


#define PADDINGTIME 50


struct Path
{
	Location start, finish;
	Location origin;
	char id;
};

vector<vector<Location>> g_moves;

//constructor
Bot::Bot()
{

};

//plays a single game of Ants.
void Bot::playGame()
{
	//reads the game parameters and sets up
	cin >> state;
	state.setup();
	endTurn();

	//continues making moves while the game is not over
	while(cin >> state)
	{
		state.updateVisionInformation();
		makeMoves();
		endTurn();
	}
};

void Bot::moveDirection(const Location& antLoc, int dir)
{
	bool foundEmpty = false;
	for (size_t i = 0 ; i < TDIRECTIONS ; i++)
	{
		Location newLoc = state.getLocation(antLoc, (dir + i) % TDIRECTIONS);
		if (!isOccupied(newLoc) && !state.grid[newLoc.row][newLoc.col].isWater && !state.grid[newLoc.row][newLoc.col].isFood)
		{
			foundEmpty = true;
			g_moves[newLoc.row][newLoc.col] = antLoc;
			break;
		}
	}
	if (!foundEmpty)
	{
		// Set the ant's square to occupied since it will not be moving
		if (!isOccupied(antLoc))
		{
			g_moves[antLoc.row][antLoc.col] = antLoc;
		}
		else
		{
			// There is a conflict, we have to make the ant trying to move here go elsewhere or stay still.
			Location conflictLoc = g_moves[antLoc.row][antLoc.col];
			while (conflictLoc != Location(-1,-1))
			{
				bool subFoundEmpty = false;
				for (size_t i = 0 ; i < TDIRECTIONS ; i++)
				{
					Location newLoc = state.getLocation(antLoc, i);
					if (!isOccupied(newLoc) && !state.grid[newLoc.row][newLoc.col].isWater && !state.grid[newLoc.row][newLoc.col].isFood)
					{
						subFoundEmpty = true;
						g_moves[newLoc.row][newLoc.col] = antLoc;
						break;
					}
				}
				if (!subFoundEmpty)
				{
					if (!isOccupied(antLoc))
					{
						g_moves[conflictLoc.row][conflictLoc.col] = conflictLoc;
						break;
					}
					else 
					{
						Location tempLoc = g_moves[conflictLoc.row][conflictLoc.col];
						g_moves[conflictLoc.row][conflictLoc.col] = conflictLoc;
						conflictLoc = tempLoc;
					}
				}
			}
		}
	}
}

void Bot::moveLocation(const Location& antLoc, const Location& targetLoc)
{
	int dir = state.direction(antLoc, targetLoc);
	moveDirection(antLoc, dir);
}

bool Bot::isOccupied(const Location& loc) const
{
	return g_moves[loc.row][loc.col].row != -1;
}

//makes the bots moves for the turn
void Bot::makeMoves()
{
#if DEBUG
	state.bug << "turn " << state.turn << ":" << endl;
	state.bug << "rows: " << state.rows << ", cols: " << state.cols << endl;
	state.bug << state << endl;
#endif

	if (state.myAnts.size() == 0)
		return;

	// Find out what player number we are so we know which ants are ours.
	int self;
	{
		Location loc = state.myAnts[0];
		self = state.grid[loc.row][loc.col].ant;
	}

	g_moves = vector<vector<Location>>(state.rows, vector<Location>(state.cols, Location(-1, -1)));


	// BFS from all food simultaneously to find closest ant to send to it.
	{
		vector<vector<vector<char>>> searched(state.rows, vector<vector<char>>(state.cols, vector<char>(state.food.size(), 0)));

		list<Path> frontier;
		// Push all the locations of the food
		for (size_t i = 0 ; i < state.food.size() ; i++)
		{
			Location startLoc = state.food[i];
			Path start;
			start.origin = start.start = start.finish = startLoc;
			start.id = i;
			frontier.push_back(start);
			searched[startLoc.row][startLoc.col][i] = 1;
		}
		
		while (!frontier.empty())
		{
			Path mv = frontier.front();
			frontier.pop_front();
			
			// Check if we've already found an ant for the food this path is for
			if (state.grid[mv.origin.row][mv.origin.col].foodHasAnt)
				continue;

			// Check if square is an ant
			Square* sq = &state.grid[mv.finish.row][mv.finish.col];
			if (sq->ant == self && !sq->antHasOrders)
			{
				moveLocation(mv.finish, mv.start);
				sq->antHasOrders = true;
				state.grid[mv.origin.row][mv.origin.col].foodHasAnt = true;
				continue;
			}

			// If we haven't found a goal node, add all the adjacent nodes that haven't been searched.
			for (int dir = 0 ; dir < TDIRECTIONS ; dir++)
			{
				Location neighbour = state.getLocation(mv.finish, dir);
				if (searched[neighbour.row][neighbour.col][mv.id] == 0 && !state.grid[neighbour.row][neighbour.col].isWater && !isOccupied(neighbour))
				{
					Path newMove;
					newMove.id = mv.id;
					newMove.origin = mv.origin;
					newMove.start = mv.finish;
					newMove.finish = neighbour;
					frontier.push_back(newMove);
					searched[neighbour.row][neighbour.col][newMove.id] = 1;
				}
			}
		}
	}


	// BFS from each ant to find closest unexplored square
	for (size_t i = 0 ; i < state.myAnts.size() ; i++)
	{
		// Skip this ant if it already has orders.
		if (state.grid[state.myAnts[i].row][state.myAnts[i].col].antHasOrders)
			continue;

		// Keep array to track which locations we have already checked.
		vector<vector<char>> searched(state.rows, vector<char>(state.cols, 0));
		
		list<Path> frontier;
		Location startLoc = state.myAnts[i];
		Path start;
		start.finish = start.start = start.origin = startLoc;
		frontier.push_back(start);
		searched[startLoc.row][startLoc.col] = 1;

		while (!frontier.empty())
		{
			Path pt = frontier.front();
			frontier.pop_front();
			Location loc = pt.finish;

			// Check if square is food (or if we see no food if it is unseen)
			Square* sq = &state.grid[loc.row][loc.col];
			if ((!sq->isVisible && state.lastVisited[loc.row][loc.col] > 5) || (sq->hillPlayer != -1 && sq->hillPlayer != self))
			{
				moveLocation(pt.origin, pt.start);
				state.grid[pt.origin.row][pt.origin.col].antHasOrders = true;
				break;
			}

			// If we haven't found a goal node, add all the adjacent nodes that haven't been searched
			for (int dir = 0 ; dir < TDIRECTIONS ; dir++)
			{
				Location neighbour = state.getLocation(loc, dir);
				if (searched[neighbour.row][neighbour.col] == 0 && !state.grid[neighbour.row][neighbour.col].isWater && !isOccupied(neighbour))
				{
					Path newPath;
					newPath.origin = pt.origin;
					if (pt.start == pt.origin)
						newPath.start = neighbour;
					else
						newPath.start = pt.start;
					newPath.finish = neighbour;

					frontier.push_back(newPath);
					searched[neighbour.row][neighbour.col] = 1;
				}
			}
		}

		// Break prematurely if we are running out of time.
		if (state.turntime - state.timer.getTime() < PADDINGTIME)
		{
#if DEBUG
			state.bug << "ENDED DUE TO RISK OF TIMEOUT" << endl;
#endif
			break;
		}
	}

	// Execute the moves.
	for (size_t i = 0 ; i < g_moves.size() ; i++)
	{
		for (size_t j = 0 ; j < g_moves[i].size() ; j++)
		{
			if (g_moves[i][j].row != -1)
			{
				Location locTo = Location(i, j);
				Location locFrom = g_moves[i][j];
				int dir = state.direction(locFrom, locTo);
				state.makeMove(locFrom, dir);
			}
		}
	}
	
#if DEBUG
	state.bug << "time taken: " << state.timer.getTime() << "ms" << endl << endl;
#endif
}

//finishes the turn
void Bot::endTurn()
{
	if(state.turn > 0)
		state.reset();
	state.turn++;

	cout << "go" << endl;
};
